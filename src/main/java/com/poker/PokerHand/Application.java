package com.poker.PokerHand;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.Collections;

@SpringBootApplication
public class Application implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(Application.class);
		app.setBannerMode(Banner.Mode.OFF);
		app.run(args);
	}

	private static Logger logger = LoggerFactory.getLogger(Application.class);

	@Override
	public void run(String... strings) throws Exception {

		ArrayList<PokerHand> hands = new ArrayList<>();

		try {
			hands.add(new PokerHand("KS QS JS TS 9S")); // Стрит-флэш
			hands.add(new PokerHand("2H 4H 6H 8H TH")); // Флэш
			hands.add(new PokerHand("5D 4S 3C 6H 7C")); // Стрит
			hands.add(new PokerHand("2D 2H 2S 7C 9D")); // Сет
			hands.add(new PokerHand("QC QS 6H 3C 2S")); // Одна пара
			hands.add(new PokerHand("TS JS QS KS AS")); // Флеш рояль
			hands.add(new PokerHand("9D 8S 7H 5C 2C")); //Высшая карта
			hands.add(new PokerHand("3D 3S 5D 5S 5H")); // Фулл хаус
			hands.add(new PokerHand("5C 5D 7S 7H 9H")); //Две пары
			hands.add(new PokerHand("2S 2D 5C 2H 2C")); // Каре
			hands.add(new PokerHand("TD 8S 7H 5C 2C")); //Высшая карта


			logger.info("Hands created successfully!");
		} catch (IllegalArgumentException e) {
			logger.error("Error creating hand: " + e.getMessage());
		}

		Collections.sort(hands, Collections.reverseOrder());

		for (PokerHand hand : hands) {
			logger.info(hand.toString());
		}
	}
}
