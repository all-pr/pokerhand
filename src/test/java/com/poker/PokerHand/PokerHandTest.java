package com.poker.PokerHand;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

class PokerHandTest {



    @Test
    void  isOnePairTest() {
        String[] cards = new String[] {"2S", "2D", "5S", "4D", "3C"};
        int pairCount = 0;
        int[] rankValues = getSortedRankValues(cards);
        for (int i = 0; i < rankValues.length - 1; i++) {
            if (rankValues[i] == rankValues[i + 1]) {
                pairCount++;
                i++;
            }
        }

        Assertions.assertEquals(1, pairCount);

    }

    @Test
    void  isOnePair2Test() {
        String[] cards = new String[] {"2S", "2D", "5S", "4D", "5C"};
        int pairCount = 0;
        int[] rankValues = getSortedRankValues(cards);
        for (int i = 0; i < rankValues.length - 1; i++) {
            if (rankValues[i] == rankValues[i + 1]) {
                pairCount++;
                i++;
            }
        }

        Assertions.assertEquals(2, pairCount);

    }

    @Test
    void  isTwoPairTest() {
        String[] cards = new String[] {"2S", "2D", "5S", "4D", "4C"};
        int pairCount = 0;
        int[] rankValues = getSortedRankValues(cards);
        for (int i = 0; i < rankValues.length - 1; i++) {
            if (rankValues[i] == rankValues[i + 1]) {
                pairCount++;
                i++;
            }
        }

        Assertions.assertEquals(2, pairCount);

    }

    @Test
    void  isSetTest() {

        String[] cards = new String[] {"2S", "3D", "2H", "6C", "2C"};

        Map<Character, Integer> cardCountMap = new HashMap<>();

        for (String card : cards) {
            char value = card.charAt(0);
            cardCountMap.put(value, cardCountMap.getOrDefault(value, 0) + 1);
        }

        for (int count : cardCountMap.values()) {
            if (count == 3) {
                Assertions.assertEquals(3, count);
            }
        }
    }

    @Test
    void  isSet2Test() {

        String[] cards = new String[] {"2S", "2D", "7H", "2H", "3C"};

        Map<Character, Integer> cardCountMap = new HashMap<>();

        for (String card : cards) {
            char value = card.charAt(0);
            cardCountMap.put(value, cardCountMap.getOrDefault(value, 0) + 1);
        }

        for (int count : cardCountMap.values()) {
            if (count == 3) {
                Assertions.assertEquals(3, count);
            }
        }
    }

    @Test
    void isStraightTest() {
        boolean result = true;
        String[] cards = new String[] {"2D", "4S", "3H", "5C", "6S"};
        int[] rankValues = getSortedRankValues(cards);
        for (int i = 1; i < rankValues.length; i++) {
            if (rankValues[i] != rankValues[i - 1] + 1) {
                result = false;
            }
        }
        Assertions.assertTrue(result);
    }

    @Test
    void isFlushTest() {

        boolean result = false;
        String[] cards = new String[] {"2D", "4D", "9D", "5D", "TD"};

        char suit = cards[0].charAt(1);
        if (Arrays.stream(cards).allMatch(card -> card.charAt(1) == suit)){
            result = true;
        }
        Assertions.assertTrue(result);
    }

    @Test
    void isFlush2Test() {

        boolean result = false;
        String[] cards = new String[] {"2D", "4S", "9D", "5D", "TD"};

        char suit = cards[0].charAt(1);
        if (Arrays.stream(cards).allMatch(card -> card.charAt(1) == suit)){
            result = true;
        }
        Assertions.assertFalse(result);
    }

    @Test
    void isFullHouseTest(){

        boolean result = false;
        String[] cards = new String[] {"2S", "2D", "7H", "2H", "7C"};

        if (isSet(cards) && isTwoPair(cards)){
            result = true;
        }
        Assertions.assertTrue(result);
    }

    @Test
    void isFullHouse2Test(){

        boolean result = false;
        String[] cards = new String[] {"2S", "8D", "7H", "2H", "7C"};

        if (isSet(cards) && isTwoPair(cards)){
            result = true;
        }
        Assertions.assertFalse(result);
    }

    @Test
    void isKingTest() {

        String[] cards = new String[] {"2S", "8D", "2D", "2H", "2C"};
        boolean result = false;
        Map<Character, Integer> cardCountMap = new HashMap<>();

        for (String card : cards) {
            char value = card.charAt(0);
            cardCountMap.put(value, cardCountMap.getOrDefault(value, 0) + 1);
        }

        for (int count : cardCountMap.values()) {

            if (count == 4) {
                result = true;
            }
        }

        Assertions.assertTrue(result);
    }

    @Test
    void isStraightFlushTest(){

        boolean result = false;
        String[] cards = new String[] {"2D", "4D", "3D", "5D", "6D"};
        if (isStraight(cards) && isFlush(cards)){
            result = true;
        }

        Assertions.assertTrue(result);
    }

    @Test
    void isRoyalFlushTest() {
        boolean result = false;
        String[] cards = new String[] {"TS", "JS", "QS", "KS", "AS"};
        int[] rankValues = getSortedRankValues(cards);
        if ((Arrays.equals(rankValues, new int[]{10, 11, 12, 13, 14})) && isFlush(cards)){
            result = true;
        }

        Assertions.assertTrue(result);
    }

    @Test
    void highestRankTest(){

        String[] lowRank = new String[] {"3D", "8S", "7H" ,"5C", "2C"};
        String[] highRank = new String[] {"TD", "8S", "7H" ,"5C", "2C"};

        int lowRankValue = getHighestCardRank(lowRank);
        int highRankValue = getHighestCardRank(highRank);

        int result = Integer.compare(highRankValue, lowRankValue);

        Assertions.assertEquals(1, result);

    }

    private int getHighestCardRank(String[] cards) {
        return getSortedRankValues(cards)[cards.length - 1];
    }


    private int[] getSortedRankValues(String[] cards) {
        int[] rankValues = new int[cards.length];
        for (int i = 0; i < cards.length; i++) {
            char rank = cards[i].charAt(0);
            rankValues[i] = "23456789TJQKA".indexOf(rank) + 2;
        }
        Arrays.sort(rankValues);
        return rankValues;
    }

    public boolean isSet(String[] cards) {

        Map<Character, Integer> cardCountMap = new HashMap<>();

        for (String card : cards) {
            char value = card.charAt(0);
            cardCountMap.put(value, cardCountMap.getOrDefault(value, 0) + 1);
        }

        for (int count : cardCountMap.values()) {
            if (count == 3) {
                return true;
            }
        }

        return false;
    }

    private boolean isTwoPair(String[] cards) {
        int pairCount = 0;
        int[] rankValues = getSortedRankValues(cards);
        for (int i = 0; i < rankValues.length - 1; i++) {
            if (rankValues[i] == rankValues[i + 1]) {
                pairCount++;
                i++;
            }
        }
        return pairCount == 2;
    }

    private boolean isFlush(String[] cards) {
        char suit = cards[0].charAt(1);
        return Arrays.stream(cards).allMatch(card -> card.charAt(1) == suit);
    }

    private boolean isStraight(String[] cards) {
        int[] rankValues = getSortedRankValues(cards);
        for (int i = 1; i < rankValues.length; i++) {
            if (rankValues[i] != rankValues[i - 1] + 1) {
                return false;
            }
        }
        return true;
    }

}