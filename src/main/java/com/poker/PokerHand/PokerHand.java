package com.poker.PokerHand;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;


public class PokerHand implements Comparable<PokerHand>{
    private static Logger logger = LoggerFactory.getLogger(PokerHand.class);

    private String[] cards;

    public PokerHand(String handString) {
        this.cards = handString.split(" ");
        validateInput();
    }

    private void validateInput() {
        if (cards.length != 5) {
            throw new IllegalArgumentException("Invalid number of cards. Must be 5.");
        }

        Set<String> uniqueCards = new HashSet<>();

        for (String card : cards) {
            if (card.length() != 2 || !isValidCard(card) || !uniqueCards.add(card)) {
                throw new IllegalArgumentException("Invalid card: " + card);
            }
        }
    }

    private boolean isValidCard(String card) {
        char rank = card.charAt(0);
        char suit = card.charAt(1);
        return isValidRank(rank) && isValidSuit(suit);
    }


    private boolean isValidRank(char rank) {
        return "23456789TJQKA".indexOf(rank) != -1;
    }

    private boolean isValidSuit(char suit) {
        return "SHDC".indexOf(suit) != -1;
    }

    @Override
    public int compareTo(PokerHand otherHand) {

        if (calculateHandRank() != otherHand.calculateHandRank()){
            return Integer.compare(calculateHandRank(), otherHand.calculateHandRank());
        }

        return Integer.compare(calculateHighestHandRank(), otherHand.calculateHighestHandRank());

    }

    private int calculateHighestHandRank(){
        return getHighestCardRank();
    }

    private int calculateHandRank() {
        if (isRoyalFlush() && isFlush()){
            return 9;
        }else if (isFlush() && isStraight()){
            return 8;
        }else if (isKing()){
            return 7;
        } else if (isSet() && isTwoPair()){
            return 6;
        }else if (isFlush()){
            return 5;
        } else if (isStraight()){
            return 4;
        } else if (isSet()){
            return 3;
        } else if (isTwoPair()) {
            return 2;
        } else if (isOnePair()) {
            return 1;
        } else {
            return 0;
        }
    }

    private boolean isRoyalFlush() {
        int[] rankValues = getSortedRankValues();
        return Arrays.equals(rankValues, new int[]{10, 11, 12, 13, 14});
    }

    public boolean isKing() {

        Map<Character, Integer> cardCountMap = new HashMap<>();

        for (String card : cards) {
            char value = card.charAt(0);
            cardCountMap.put(value, cardCountMap.getOrDefault(value, 0) + 1);
        }

        for (int count : cardCountMap.values()) {
            if (count == 4) {
                return true;
            }
        }

        return false;
    }

    private boolean isFlush() {
        char suit = cards[0].charAt(1);
        return Arrays.stream(cards).allMatch(card -> card.charAt(1) == suit);
    }

    private boolean isStraight() {
        int[] rankValues = getSortedRankValues();
        for (int i = 1; i < rankValues.length; i++) {
            if (rankValues[i] != rankValues[i - 1] + 1) {
                return false;
            }
        }
        return true;
    }

    public boolean isSet() {

        Map<Character, Integer> cardCountMap = new HashMap<>();

        for (String card : cards) {
            char value = card.charAt(0);
            cardCountMap.put(value, cardCountMap.getOrDefault(value, 0) + 1);
        }

        for (int count : cardCountMap.values()) {
            if (count == 3) {
                return true;
            }
        }

        return false;
    }

    private boolean isTwoPair() {
        int pairCount = 0;
        int[] rankValues = getSortedRankValues();
        for (int i = 0; i < rankValues.length - 1; i++) {
            if (rankValues[i] == rankValues[i + 1]) {
                pairCount++;
                i++;
            }
        }
        return pairCount == 2;
    }

    private boolean isOnePair() {
        int pairCount = 0;
        int[] rankValues = getSortedRankValues();
        for (int i = 0; i < rankValues.length - 1; i++) {
            if (rankValues[i] == rankValues[i + 1]) {
                pairCount++;
                i++;
            }
        }
        return pairCount == 1;
    }

    private int getHighestCardRank() {
        return getSortedRankValues()[cards.length - 1];
    }




    private int[] getSortedRankValues() {
        int[] rankValues = new int[cards.length];
        for (int i = 0; i < cards.length; i++) {
            char rank = cards[i].charAt(0);
            rankValues[i] = "23456789TJQKA".indexOf(rank) + 2;
        }
        Arrays.sort(rankValues);
        return rankValues;
    }

    @Override
    public String toString() {
        return "PokerHand{" + "cards=" + Arrays.toString(cards) + '}';
    }
}

